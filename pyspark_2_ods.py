from pyspark.sql import SparkSession
from dateutil.parser import parse
from pyspark.sql.functions import udf
from pyspark.sql.functions import explode
from pyspark.sql.functions import lit
from pyspark.sql.types import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import traceback

import time
import os.path
import datetime
import re
import configparser
import logging
import sys, smtplib, cgitb

import threading


def load_table_2_ods( mapping_line, load_info, spark, config, logger, result_arr ):

    def get_count_src( mapping_info, load_info ):
        logger.debug( log_header + "    Entering get_count_src" )

        count_src = -1
        try:
            mssql_count_src_query = f"""
                (
                    SELECT
                        COUNT(*) as count_src
                    FROM {mapping_info["src_table_path"]}
                ) count_src
                """

            mssql_count_src_query_df = spark.read.jdbc(
                  url = load_info["src_url"]
                , table = mssql_count_src_query
                , properties = load_info["mssql_properties"]
                )

            count_src = mssql_count_src_query_df.first().count_src
            logger.debug( log_header + f"    count_src: {count_src}" )

            if count_src <= 0:
                logger.warn(log_header + "No records found in source table")

            return count_src

        except Exception as e:
            # Log error but don't raise further as the other tables should still load if this fails
            if count_src == 0:
                error_msg = str(e)
            else:
                error_msg = "Source table not found: " + mapping_info["src_table_path"]
            logger.error( log_header + error_msg )
            raise Exception(error_msg)

    def get_tgt_record_num( mapping_info, load_info):
        logger.debug( log_header + "    Entering get_tgt_record_num" )

        try:
            hive_tgt_count_query = f"""
                SELECT
                    COUNT(*) as tgt_count
                FROM {mapping_info["tgt_table_path"]}
                """
            tgt_count = spark.sql( hive_tgt_count_query ).first().tgt_count
            logger.debug( log_header + f"    tgt_count: {tgt_count}" )

            return tgt_count

        except Exception as e:
            logger.debug( log_header + str(e) )
            logger.debug("get_tgt_record_num Exception:")
            # If error in hive query, assume table doesn't exist, so no rows
            return 0

    def table_exists_in_tgt( tgt_table, existing_tables_arr ):
        logger.debug( log_header + "      Entering table_exists_in_tgt" )

        for tablename in existing_tables_arr:
            if tgt_table == tablename:
                logger.debug( log_header + f"      Table {tgt_table} exists in datalake'" )
                return True

        logger.debug( log_header + f"      Table {tgt_table} does not exist in datalake'" )
        return False

    def build_import_dataframe( mssql_load_query, load_info ):
        logger.debug( log_header + "    Entering build_import_dataframe" )

        try:
            mssql_load_df = spark.read.jdbc(
                  url        = load_info["src_url"]
                , table      = mssql_load_query
                , properties = load_info["mssql_properties"]
                )

            # rename columns with invalid characters
            tran_tab = str.maketrans({x: None for x in list('{()}')})
            cols_renamed_df = mssql_load_df.toDF(*(re.sub(r'[\.\s]+', '_', c)
                .translate(tran_tab) for c in mssql_load_df.columns))

            # add audit columns
            batch_id_df = cols_renamed_df.withColumn("ods_batch_id", lit(load_info['batch_id']))
            load_ts_df  = batch_id_df.withColumn("ods_load_time", lit(load_info['batch_load_ts']))

            return load_ts_df

        except Exception as e:
            logger.error( log_header + str(e) )
            error_msg = "ERROR in build_import_dataframe for: " + mapping_info["src_table_path"]
            raise Exception(error_msg)

    def write_load_to_datalake(mssql_load_df, load_info, mapping_info ):
        logger.debug( log_header + "    Entering write_load_to_datalake" )

        try:
            load_count = mssql_load_df.count()
            logger.info( log_header + f"  Loading {load_count} records to ODS" )

            tgt_table = mapping_info["target_table"]
            tmp_load_tablename = tgt_table + "_load"
            mssql_load_df.createOrReplaceTempView( tmp_load_tablename )

            # use diferent load query depending on if the table already exists or not
            if table_exists_in_tgt( tgt_table, load_info["existing_tables_arr"] ):
                hive_load_table_query = f"""
                    INSERT INTO {mapping_info["tgt_table_path"]}
                    SELECT * FROM {tmp_load_tablename}
                    """
            else:
                hive_load_table_query = f"""
                    CREATE TABLE {mapping_info["tgt_table_path"]}
                        STORED AS PARQUET TBLPROPERTIES ('parquet.compression'='GZIP') as 
                    SELECT * FROM {tmp_load_tablename}
                    """
            logger.debug("hive_load_table_query:")
            logger.debug(hive_load_table_query)

            spark.sql( hive_load_table_query )

            # drop tempView
            spark.catalog.dropTempView( tmp_load_tablename )

        except Exception as e:
            logger.error( log_header + str(e) )
            error_msg = "ERROR in write_load_to_datalake for: " + mapping_info["tgt_table_path"]
            raise Exception(error_msg)

    def get_count_loaded( mapping_info, load_info ):
        logger.debug( log_header + "    Entering get_count_loaded" )

        try:
            hive_records_inserted_query = f"""
                SELECT
                    count(*) as insert_count
                FROM {mapping_info["tgt_table_path"]}
                WHERE 1=1
                    AND ods_batch_id = {load_info['batch_id']}
                """
            hive_records_inserted_df = spark.sql( hive_records_inserted_query )
            count_loaded     = hive_records_inserted_df.first().insert_count
            logger.info( log_header + f"  LOADED {count_loaded} records to ODS" )

            return count_loaded

        except Exception as e:
            logger.error( log_header + str(e) )
            error_msg = "ERROR in get_count_loaded for " + mapping_info["src_table_path"]
            raise Exception(error_msg)

    # =============================================================================================

    def format_mapping_info( mapping_line, load_info, logger ):
        src_db     = load_info["src_db"]
        src_schema = mapping_line.source_db_schema
        src_table  = mapping_line.source_table
        src_table_path = f"{src_db}.{src_schema}.{src_table}"
        tgt_table_path = f"{load_info['tgt_db']}.{mapping_line.target_table}"

        def format_incremental_ts( incremental_ts ):
            try:
                if incremental_ts is None:
                    return None
                else:
                    millisec_removed = incremental_ts[0:19]
                    return datetime.datetime.strptime(millisec_removed, '%Y-%m-%d %H:%M:%S')
            except Exception as e:
                # Log error but don't raise further as the other tables should still load if this fails
                error_msg = "ERROR in format_incremental_ts for target: " + mapping_line.target_table
                logger.error( error_msg )
                logger.error( str(e) )

                raise Exception(error_msg)

        try:
            mapping_info = {
                "source_db_schema"         : mapping_line.source_db_schema
              , "source_table"             : mapping_line.source_table
              , "src_table_path"           : src_table_path

              , "target_table"             : mapping_line.target_table.lower()
              , "tgt_table_path"           : tgt_table_path.lower()

              , "load_type"                : mapping_line.load_type

                # incremental_datetime_col can be null for trunc, so set to empty string if null
              , "incremental_datetime_col" : mapping_line.incremental_datetime_col or ''
              , "incremental_start_ts"     : format_incremental_ts(mapping_line.incr_start_ts)
              , "incremental_end_ts"       : format_incremental_ts(mapping_line.incr_end_ts)
            }
            return mapping_info
        except Exception as e:
            raise Exception(e)

    def truncate_reload_table( mapping_info, load_info, load_result):
        logger.info( log_header + "  TRUNCATE reloading " + mapping_info["src_table_path"] )

        def rename_existing_tgt_table( mapping_info ):
            logger.debug( log_header + "    Entering rename_existing_tgt_table" )
            tgt_table_path = mapping_info["tgt_table_path"]
            tgt_table_bk = tgt_table_path + "_bk_trunc_reload"

            try:
                # DROP previous backup table IF EXISTS.
                hive_drop_backup_table_query = f"DROP TABLE IF EXISTS {tgt_table_bk}"
                # logger.debug( "      hive_drop_backup_table_query: " )
                # logger.debug( hive_drop_backup_table_query )
                spark.sql( hive_drop_backup_table_query )

                # RENAME the table name.
                hive_rename_query = f"ALTER TABLE {tgt_table_path} RENAME TO {tgt_table_bk}"
                # logger.debug( "      hive_rename_query: " )
                # logger.debug( hive_rename_query )
                spark.sql( hive_rename_query )

                # RECREATE original table
                hive_recreate_query = f"""
                    CREATE TABLE {tgt_table_path}
                    SELECT *
                    FROM {tgt_table_bk}
                    WHERE 1=2 -- create structure only, no data
                    """
                spark.sql( hive_recreate_query )

            except Exception as e:
                error_msg = "ERROR in rename_existing_tgt_table for: " + tgt_table_path
                logger.error( log_header + str(e) )
                raise Exception(error_msg)

        try:
            load_result["count_src"] = get_count_src( mapping_info, load_info )
            load_result["count_tgt_start"] = get_tgt_record_num( mapping_info, load_info)

            src_table_path = mapping_info["src_table_path"]
            mssql_load_df = build_import_dataframe( src_table_path, load_info )

            tgt_table = mapping_info["target_table"]

            if table_exists_in_tgt(tgt_table, load_info["existing_tables_arr"] ):
                rename_existing_tgt_table( mapping_info )

            write_load_to_datalake( mssql_load_df, load_info, mapping_info )

            load_result["count_tgt_end"] = get_tgt_record_num( mapping_info, load_info)
            load_result["count_loaded"]  = get_count_loaded( mapping_info, load_info )

            load_result["load_status"] = "SUCCESS"

            logger.info( log_header + "  SUCCESSFUL TRUNCATE reload" )

        except Exception as e:
            # Log error but don't raise further as the other tables should still load if this fails
            error_msg = log_header + "  FAILED: TRUNCATE reload"
            logger.error( error_msg )
            load_result["load_status"] = "FAILURE"
            load_result["failure_reason"] = str(e)

            raise Exception(e)

    def incremental_load_table( mapping_info, load_info, load_result ):
        logger.info( log_header + "  INCREMENTAL loading " + mapping_info["src_table_path"] )

        def check_incremental_column( mapping_info, load_info ):
            logger.debug( log_header + "    Entering check_incremental_column")

            incremental_col = mapping_info["incremental_datetime_col"]
            src_table_path  = mapping_info["src_table_path"]
            mssql_incremental_col_query = f"""
                (
                    SELECT TOP 1
                        {incremental_col} as incremental_col
                    FROM {src_table_path}
                ) incremental_col
                """

            try:
                logger.debug(log_header + 'mssql_incremental_col_query')
                logger.debug(log_header + mssql_incremental_col_query)
                mssql_incremental_col_df = spark.read.jdbc(
                      url = load_info["src_url"]
                    , table = mssql_incremental_col_query
                    , properties = load_info["mssql_properties"]
                    )
                # Check if select incremental columnn returns incremntal_col
                incremental_col_val = mssql_incremental_col_df.first().incremental_col
                logger.debug( log_header + "    Valid incremental_datetime_col" )

            except Exception as e:
                logger.error( log_header + str(e) )
                error_msg = "ERROR in check_incremental_column for " + src_table_path
                raise Exception(error_msg)

        def get_incremental_start_ts( mapping_info, load_info ):
            logger.debug( log_header + "    Entering get_incremental_start_ts")

            start_ts = datetime.datetime.strptime("1900-01-01 00:00:00", '%Y-%m-%d %H:%M:%S')

            if mapping_info["incremental_start_ts"] is not None:
                start_ts = mapping_info["incremental_start_ts"]
            else:
                for loadtime_row in load_info["latest_load_times"]:
                    if loadtime_row.target_table == mapping_info["target_table"]:
                        start_ts = loadtime_row.latest_incr_loadtime

            logger.debug(log_header + '      start_ts: ' + str(start_ts))
            return start_ts

        def get_incremental_end_ts( mapping_info, load_info ):
            logger.debug( log_header + "    Entering get_incremental_end_ts")

            end_ts = load_info["batch_load_ts"]
            if mapping_info["incremental_end_ts"] is not None:
                end_ts = mapping_info["incremental_end_ts"]

            logger.debug(log_header + '      end_ts: ' + str(end_ts))
            return end_ts

        def build_incremental_query( mapping_info, incremental_start_ts, incremental_end_ts ):
            logger.debug( log_header + "    Entering build_incremental_query")

            src_table_path       = mapping_info["src_table_path"]
            incremental_col      = mapping_info["incremental_datetime_col"]
            incremental_mssql_query = f"""
                (
                    SELECT *
                    FROM {src_table_path}
                    WHERE 1=1
                      AND {incremental_col} >= '{incremental_start_ts}'
                      AND {incremental_col} <  '{incremental_end_ts}'
                ) incremental_load
                """

            logger.debug(log_header + ' incremental_mssql_query')
            logger.debug(log_header + incremental_mssql_query)
            return incremental_mssql_query

        try:
            load_result["count_src"] = get_count_src( mapping_info, load_info )
            load_result["count_tgt_start"] = get_tgt_record_num( mapping_info, load_info)

            check_incremental_column( mapping_info, load_info )

            load_result["incremental_start_ts"] = get_incremental_start_ts(mapping_info, load_info)
            load_result["incremental_end_ts"]   = get_incremental_end_ts( mapping_info, load_info )

            mssql_load_query = build_incremental_query( mapping_info
                , incremental_start_ts, incremental_end_ts )
            mssql_load_df = build_import_dataframe( mssql_load_query, load_info )
            write_load_to_datalake( mssql_load_df, load_info, mapping_info )

            load_result["count_tgt_end"] = get_tgt_record_num( mapping_info, load_info)
            load_result["count_loaded"]  = get_count_loaded( mapping_info, load_info )
            load_result["load_status"]   = "SUCCESS"

            logger.info( log_header + "  SUCCESSFUL INCREMENTAL load" )

        except Exception as e:
            error_msg = log_header + "  FAILED: INCREMENTAL load"
            logger.error( error_msg )
            load_result["load_status"] = "FAILURE"
            load_result["failure_reason"] = str(e)

            raise Exception( e )

    def handle_invalid_load_type( load_result ):
        error_msg = "Invalid load_type for table: " + mapping_line.source_table
        logger.error( error_msg )

        load_result["load_status"]    = "INVALID"
        load_result["failure_reason"] = error_msg

    def set_load_end_and_runtimes( load_result ):
        process_end_ts = datetime.datetime.now()
        process_runtime_sec = (process_end_ts - load_result["process_start_ts"]).total_seconds()

        load_result["process_end_ts"]      = process_end_ts
        load_result["process_runtime_sec"] = int(process_runtime_sec)

    # =============================================================================================

    def update_load_history_table( load_result, load_info, mapping_info ):
        logger.debug( log_header + "Entering update_load_history_table" )

        def create_load_row_df( load_result, load_info, mapping_info, spark, logger ):
            logger.debug(log_header + 'load_history_tuple')
            load_history_tuple = [(
                  load_info.get("batch_id")
                , load_info["job_name"]
                , load_info.get("batch_load_ts")
                , mapping_info.get("load_type")
                , load_info.get("src_db")
                , mapping_info.get("source_db_schema")
                , mapping_info.get("source_table")
                , load_info.get("tgt_db")
                , mapping_info.get("target_table")
                , mapping_info.get("incremental_datetime_col")
                , load_info.get("src_server")
                , load_info.get("src_port")
                , load_info.get("source_user")
                , load_result.get("count_src")
                , load_result.get("count_tgt_start")
                , load_result.get("count_tgt_end")
                , load_result.get("count_loaded")
                , load_result.get("process_start_ts")
                , load_result.get("process_end_ts")
                , load_result.get("process_runtime_sec")
                , load_result.get("incremental_start_ts")
                , load_result.get("incremental_end_ts")
                , load_result.get("load_status") or ""
                , load_result.get("load_status") or ""
                , load_result.get("failure_reason") or ""
                )]
            logger.debug(log_header + str(load_history_tuple) )

            load_history_schema = StructType([
                  StructField("batch_id", IntegerType(), True)
                , StructField("job_name", StringType(), True)
                , StructField("batch_load_ts", TimestampType(), True)
                , StructField("load_type", StringType(), True)
                , StructField("source_db", StringType(), True)
                , StructField("source_schema", StringType(), True)
                , StructField("source_table", StringType(), True)
                , StructField("target_db", StringType(), True)
                , StructField("target_table", StringType(), True)
                , StructField("incremental_datetime_col", StringType(), True)
                , StructField("source_server", StringType(), True)
                , StructField("source_port", StringType(), True)
                , StructField("source_user", StringType(), True)
                , StructField("count_source", IntegerType(), True)
                , StructField("count_target_start", IntegerType(), True)
                , StructField("count_target_end", IntegerType(), True)
                , StructField("count_loaded", IntegerType(), True)
                , StructField("process_start_ts", TimestampType(), True)
                , StructField("process_end_ts", TimestampType(), True)
                , StructField("process_runtime_sec", IntegerType(), True)
                , StructField("incr_load_start_ts", TimestampType(), True)
                , StructField("incr_load_end_ts", TimestampType(), True)
                , StructField("load_status_code", StringType(), True)
                , StructField("load_status_desc", StringType(), True)
                , StructField("failure_reason", StringType(), True)
                ])

            return spark.createDataFrame(load_history_tuple, load_history_schema)

        try:
            load_row_df = create_load_row_df( load_result, load_info, mapping_info, spark, logger )
            load_row = load_row_df.collect()
            logger.debug('load_row')
            logger.debug(load_row)

            load_history_table = load_info["load_history_table"]

            load_row_df.write \
                .saveAsTable(load_history_table, format="hive", mode="append")

            logger.debug( log_header + "  LOADED load_history_table" )

            return 'LOADED load_history_table'

        except Exception as e:
            error_msg = "Error in update_load_history_table for: " + mapping_info["tgt_table_path"]
            logger.error(e)
            raise Exception(error_msg)

    def format_error_email_subject( load_info, mapping_line ):
        src_db     = load_info["src_db"]
        src_schema = mapping_line.source_db_schema
        src_table  = mapping_line.source_table
        src_table_path = f"{src_db}.{src_schema}.{src_table}"
        load_type      = mapping_line.load_type.upper()

        return f"Error running {load_type} load for Table: {src_table_path}"

    try:
        log_header = "UNKNOWN"
        load_result = {
            "tgt_table_path"  : "UNKNOWN"
          , "load_type"       : mapping_line.load_type
          , "load_status"     : "FAILURE"
          , "failure_reason"  : None

          , "count_src"       : None
          , "count_tgt_start" : None
          , "count_tgt_end"   : None
          , "count_loaded"    : None
        }

        mapping_info = format_mapping_info( mapping_line, load_info, logger )
        log_header = mapping_info["src_table_path"] + " - "
        logger.debug( log_header + "Entering load_table_2_ods" )

        load_result["tgt_table_path"]   = mapping_info.get("tgt_table_path")
        load_result["process_start_ts"] = datetime.datetime.now()

        if mapping_line.load_type == 'truncate-reload':
            truncate_reload_table( mapping_info, load_info, load_result)
        # TODO: commented out sections below
        elif mapping_line.load_type == 'incremental-datetime':
            incremental_load_table( mapping_info, load_info, load_result )
        else:
            handle_invalid_load_type( load_result )

        set_load_end_and_runtimes( load_result )
        logger.debug(log_header + "load_result")
        logger.debug(log_header + str(load_result) )

    except Exception as e:
        logger.error( log_header + str(e) )

        # Log error and send email but don't raise further because
        #   the other tables should still load even if this fails
        email_subject = format_error_email_subject( load_info, mapping_line )
        triggerEMail( config, email_subject )

    result_arr.append( load_result )
    update_load_history_table( load_result, load_info, mapping_info )

def update_control_table( result_arr, load_info, config, spark, logger ):
    logger.debug( "Entering update_control_table" )

    def process_result_arr(result_arr, logger):
        logger.debug( "Entering process_result_arr" )

        def get_summary_str( result_arr, logger ):
            logger.debug( "Entering get_summary_str" )
            load_status_summary = "UNKNOWN"
            if any( result["load_status"] == "FAILURE" for result in result_arr):
                load_status_summary = "FAILURE"
            elif any( result["load_status"] == "INVALID" for result in result_arr):
                load_status_summary = "INVALID"
            else:
                load_status_summary = "SUCCESS"
            
            logger.debug("load_status_summary")
            logger.debug(load_status_summary)

            return load_status_summary

        def get_details_str( result_arr, logger ):
            logger.debug( "Entering get_details_str" )
            load_type_dict = {}

            for result in result_arr:
                load_type = result["load_type"]
                status = result["load_status"]

                # initialise load_type if it doesn't exist
                if not load_type_dict.get(load_type):
                    load_type_dict[load_type] = {}

                # below increments the load_type status by one, 
                #   or initialises with 0 if doesn't exist
                load_type_dict[load_type][status] \
                    = (load_type_dict.get(load_type).get(status) or 0) + 1

            logger.debug("load_type_dict")
            logger.debug(load_type_dict)

            return str(load_type_dict)

        def get_reasons_str( result_arr, logger ):
            logger.debug( "Entering get_reasons_str" )

            failure_reasons_arr = []
            for result in result_arr:
                if result["failure_reason"] is not None:
                    failure_reasons_arr.append( result["failure_reason"] )

            logger.debug("failure_reasons_arr")
            logger.debug(failure_reasons_arr)

            return str(failure_reasons_arr)

        result_dict = {
              "load_status_summary" : get_summary_str(result_arr, logger)
            , "load_status_details" : get_details_str(result_arr, logger)
            , "failure_reasons"     : get_reasons_str( result_arr, logger )
            }
        logger.debug("result_dict")
        logger.debug(result_dict)

        return result_dict

    def create_control_row_df( result_dict, load_info, spark, logger ):
        logger.debug('control_table_tuple')

        process_runtime_sec = (load_info.get("job_end_ts") - load_info.get("job_start_ts")).total_seconds()
        control_table_tuple = [(
              load_info.get("batch_id")
            , load_info["job_name"]
            , load_info.get("batch_load_ts")
            , load_info.get("job_start_ts")
            , load_info.get("job_end_ts")
            , int( process_runtime_sec )
            , result_dict.get("load_status_summary")
            , result_dict.get("load_status_details")
            , result_dict.get("failure_reasons")
            )]
        logger.debug(control_table_tuple)

        logger.debug('control_table_schema')
        control_table_schema = StructType([
              StructField("batch_id", IntegerType(), True)
            , StructField("job_name", StringType(), True)
            , StructField("batch_load_ts", TimestampType(), True)
            , StructField("job_start_ts", TimestampType(), True)
            , StructField("job_end_ts", TimestampType(), True)
            , StructField("process_runtime_sec", IntegerType(), True)
            , StructField("load_status_summary", StringType(), True)
            , StructField("load_status_details", StringType(), True)
            , StructField("failure_reasons", StringType(), True)
            ])
        logger.debug(control_table_schema)

        return spark.createDataFrame(control_table_tuple, control_table_schema)

    try:
        result_dict = process_result_arr(result_arr, logger)
        control_row_df = create_control_row_df( result_dict, load_info, spark, logger )

        control_row = control_row_df.collect()
        logger.debug('control_row')
        logger.debug(control_row)

        control_table = load_info["control_table"]

        control_row_df.write \
            .saveAsTable(control_table, format="hive", mode="append")

        logger.debug( "LOADED control_table" )

        job_name    = load_info["job_name"]
        load_status = result_dict["load_status_summary"]
        email_subject = f"{job_name} ran resulting in {load_status}"
        logger.debug("email_subject")
        logger.debug(email_subject)
        triggerEMail( config, email_subject, str(result_arr) )

    except Exception as e:
        error_msg = "Error in update_control_table"
        logger.error(error_msg)
        logger.error(e)

# ====================================================================================================

def get_load_info( config, spark, logger ):

    def get_src_url( config ):
        src_server = config['MSSQL_ODBC']['source.server']
        src_port   = config['MSSQL_ODBC']['source.port']
        src_db     = config['MSSQL_ODBC']['source.database']
        src_args   = config['MSSQL_ODBC']['JDBC.Additional.Args']

        src_url = f"jdbc:jtds:sqlserver://{src_server}:{src_port};databaseName={src_db}{src_args}"

        return src_url

    def get_batch_id( config, spark ):
        logger.debug( "Entering get_batch_id" )
        try:
            etl_control_table = config['Datalake']['etl.control.table']

            hive_batchid_query   = 'select coalesce(max(batch_id) + 1, 1) as next_batch_id from ' + etl_control_table
            hive_next_batchid_df = spark.sql( hive_batchid_query )
            next_batchid         = hive_next_batchid_df.first().next_batch_id

            logger.info("Current batch id: " + str( next_batchid ) )

            return next_batchid

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get batch_id from datalake control table " + etl_control_table
            raise Exception( error_msg )

    def get_batch_load_time( config, mssql_properties ):
        logger.debug( "Entering get_batch_load_time" )
        try:
            src_url = get_src_url( config )

            # setup MS JDBC connection
            incremental_delay     = config['Incremental']['incremental.time.delay.min']
            logger.debug( "  Time Window Difference in min: " + incremental_delay )
            mssql_date_time_query = f"""
                (
                    SELECT
                        CONVERT(DATETIME2(0), dateadd(minute, - {incremental_delay}, getdate()) ) as batch_load_datetime
                ) batch_load_datetime
                """

            logger.debug("src_url:")
            logger.debug(src_url)
            logger.debug("mssql_date_time_query:")
            logger.debug(mssql_date_time_query)
            logger.debug("mssql_properties:")
            logger.debug(mssql_properties)

            mssql_curDateTime_df  = spark.read.jdbc(url = src_url, table = mssql_date_time_query, properties = mssql_properties)

            batch_load_ts_str = mssql_curDateTime_df.first().batch_load_datetime
            batch_load_ts = datetime.datetime.strptime(batch_load_ts_str, '%Y-%m-%d %H:%M:%S')
            logger.info("Current batch load end time: " + str( batch_load_ts ) )

            return batch_load_ts

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get batch_load_ts from source_db" + src_url
            raise Exception( error_msg )

    def get_latest_load_time_for_all_tables( config, spark ):
        logger.debug( "Entering get_latest_load_time_for_all_tables" )
        try:
            load_history_table                = config['Datalake']['etl.load.history']
            hive_latest_table_load_time_query = f"""
                SELECT
                    target_table
                  , max(incr_load_end_ts) OVER(Partition by target_table) as latest_incr_loadtime
                FROM {load_history_table}
                WHERE 1=1
                    AND target_table is not null
                    AND load_status_code = "SUCCESS"
                """
            logger.debug("hive_latest_table_load_time_query")
            logger.debug(hive_latest_table_load_time_query)
            hive_latest_load_times_all_tables_df = spark.sql( hive_latest_table_load_time_query )
            hive_latest_load_times_all_tables_df.dropDuplicates()
            latest_load_times = hive_latest_load_times_all_tables_df.collect()

            return latest_load_times

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get latest batch load times from datalake control table " + load_history_table
            raise Exception( error_msg )

    def get_existing_tablenames( config, spark ):
        logger.debug( "Entering get_existing_tablenames" )
        try:
            hive_get_tables_query   = f"SHOW TABLES FROM {config['Datalake']['target.database']}"
            hive_existing_tables_df = spark.sql( hive_get_tables_query )
            tablenames_rdd          = hive_existing_tables_df \
                .rdd.map( lambda row: row.tableName )
            existing_tables_arr = tablenames_rdd.collect()
            # logger.debug( "existing_tables_arr:" )
            # logger.debug( existing_tables_arr )

            return existing_tables_arr

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get existing tables"
            raise Exception( error_msg )

    logger.info('============================== Collecting metadata required for load ===============================')

    try:
        mssql_properties = {
            "user"     : config['MSSQL_ODBC']['User']
          , "password" : config['MSSQL_ODBC']['Password']
          , "driver"   : config['MSSQL_ODBC']['JDBC.Driver.Name']
        }

        load_info = {
            "mssql_properties" : mssql_properties
          , "job_name"         : config['Job']['name']

          , "src_url"    : get_src_url( config )
          , "src_server" : config['MSSQL_ODBC']['source.server']
          , "src_port"   : config['MSSQL_ODBC']['source.port']
          , "src_db"     : config['MSSQL_ODBC']['source.database']
          , "tgt_db"     : config['Datalake']['target.database'].lower()

          , "batch_id"            : get_batch_id( config, spark )
          , "batch_load_ts"       : get_batch_load_time( config, mssql_properties )
          , "latest_load_times"   : get_latest_load_time_for_all_tables( config, spark )
          , "existing_tables_arr" : get_existing_tablenames( config, spark )

          , "control_table"      : config['Datalake']['etl.control.table']
          , "load_history_table" : config['Datalake']['etl.load.history']
          , "source_user"        : config['MSSQL_ODBC']['User']
        }

        logger.debug('load_info')
        logger.debug(load_info)

        logger.info('=================================== Finished collecting metadata ===================================')

        return load_info

    except Exception as e:
        error_msg = "Unable to retrieve metadata for batch load"
        logger.error(error_msg)
        logger.error(e)
        raise Exception(e)

def threaded_load_tables_2_ods( job_start_ts, config, load_info, spark, logger ):

    def get_array_of_mapping_lines( config, spark, logger ):
        logger.debug("Entering get_array_of_mapping_lines")

        try:
            tgt_path     = config['Src_Tgt_Mapping']['mapping.path']
            tgt_file     = config['Src_Tgt_Mapping']['mapping.filename']
            path_to_file = tgt_path + tgt_file
            logger.debug("path_to_file: " + path_to_file)

            mapping_lines_arr = spark.read.csv(path_to_file, header=True).collect()
            logger.debug("tables_mappings_arr:")
            logger.debug(mapping_lines_arr)

            return mapping_lines_arr

        except Exception as e:
            error_msg = "Unable to get mapping of tables from source to ods"
            logger.error( error_msg )
            raise

    logger.info('=================================== Loading Tables using threads ===================================')

    try:
        mapping_lines_arr = get_array_of_mapping_lines( config, spark, logger )

        thread_arr = []
        result_arr = []
        for mapping_line in mapping_lines_arr:
            # set thread with parameters then let it run
            load_thread = threading.Thread(target=load_table_2_ods
                , args=(mapping_line, load_info, spark, config, logger, result_arr) )
            load_thread.start()
            thread_arr.append( load_thread )

        threads_running = True
        while threads_running > 0:
            threads_running = False
            for thread in thread_arr:
                if thread.is_alive():
                    threads_running = True

            if threads_running:
                time.sleep(1)

        load_info["job_start_ts"] = job_start_ts
        load_info["job_end_ts"]   = datetime.datetime.now()

        logger.debug("result_arr")
        logger.debug(result_arr)

        update_control_table( result_arr, load_info, config, spark, logger )

        logger.info('================================== Finished Loading Tables to ODS ==================================')

    except Exception as e:
        error_msg = "Error batch loading tables from source to ods"
        logger.error( error_msg )
        raise

# ====================================================================================================

def triggerEMail( config, email_subject, msg_text=None ):

    def sendMail(sender, recipient, subject, html, text):

        message = createMailPart(sender, recipient, subject, html, text)
        server = smtplib.SMTP(config['SMTP']['smtp.host'], config['SMTP']['smtp.port'],  config['SMTP']['smtp.time.out'])
        server.sendmail(sender, recipient, message.as_string())
        server.quit()

    # ---------------------------------------------------------------
    def createMailPart(sender, recipient, subject, html, text):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = recipient

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)

        return msg

    def create_hacky_html(email_subject, msg_text):
        return f"""
        <meta http-equiv="Content-Type" content="text/html; charset=us-ascii"><body bgcolor="#f0f0f8">
        <table width="100%" cellspacing="0" cellpadding="2" border="0" summary="heading">
        <tr bgcolor="#6622aa">
        <td valign="bottom">&nbsp;<br>
        <font color="#ffffff" face="helvetica, arial">&nbsp;<br><big><big><strong>{email_subject}</strong></big></big></font></td><td align="right" valign="bottom"><font color="#ffffff" face="helvetica, arial">Python 3.6.4: /opt/cloudera/parcels/Anaconda-5.1.0.1/bin/python<br>Thu Feb 28 10:33:39 2019</font></td></tr></table>
            
        <p>{msg_text}</p>

        """

    print('triggerEMail email_subject')
    print(email_subject)

    print('triggerEMail msg_text')
    print(msg_text)

    msg_html = cgitb.html( sys.exc_info() )
    if msg_text is None:
        msg_text = cgitb.text( sys.exc_info() )
    else:
        msg_html = create_hacky_html(email_subject, msg_text)

    sendMail( config['SMTP']['smtp.sender']
            , config['SMTP']['smtp.recipient']
            , email_subject
            , msg_html
            , msg_text
            )

# ====================================================================================================

def run_spark_job( config_file ):

    def initialise_logger( config ):

        def log_level(x):
            return {
                'DEBUG': logging.DEBUG,
                'INFO': logging.INFO,
                'WARNING': logging.WARNING,
                'ERROR': logging.ERROR,
                'CRITICAL': logging.CRITICAL
            }.get(x, logging.DEBUG)

        # create logger

        LOG_FILENAME = config['Logging']['logfile.directory'] + '/' + config['Src_Tgt_Mapping']['mapping.filename'] + '-' + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f") + '.log'
        logger = logging.getLogger( config['Job']['name'] )
        logger.setLevel( log_level( config['Logging']['console.level'] ) )

        fh = logging.FileHandler( LOG_FILENAME )
        fh.setLevel( log_level( config['Logging']['logfile.level'] ) )
        ch = logging.StreamHandler()
        ch.setLevel( log_level( config['Logging']['console.level'] ) )

        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        return (logger, LOG_FILENAME)

    def initialise_spark_session( config ):
        logger.debug( "Entering initialise_spark_session" )
        jdbc_driver_path = config['MSSQL_ODBC']['JDBC.Driver.Path']
        job_name         = config['Job']['name']

        return (SparkSession
                .builder
                .appName( job_name )
                .config('spark.executor.memory', config['SPARK']['spark.executor.memory'])
                .config('spark.driver.memory', config['SPARK']['spark.driver.memory'])
                .config("spark.cores.max", config['SPARK']['spark.cores.max'])
                .config("spark.sql.parquet.writeLegacyFormat",
                        "true")  # otherwise hive can't read parquet with Decimal(19,2) fields
                .config('spark.driver.extraClassPath', jdbc_driver_path)
                .config('spark.executor.extraClassPath', jdbc_driver_path)
                .config('spark.yarn.principal', config['SPARK']['Principal'])
                .config('spark.yarn.keytab', config['SPARK']['KeytabFile'])
                .config('spark.default.parallelism' , '4')
                .enableHiveSupport()
                .getOrCreate())

    try:
        job_start_ts = datetime.datetime.now()

        config = configparser.RawConfigParser()
        config.read(config_file)

        (logger, LOG_FILENAME) = initialise_logger( config )
        logger.info('========================================== Initialising ============================================')
        logger.info("Config file: " + config_file)
        logger.info("Logger file: " + LOG_FILENAME)

        spark  = initialise_spark_session( config )

        load_info = get_load_info( config, spark, logger )

        threaded_load_tables_2_ods( job_start_ts, config, load_info, spark, logger )

    except Exception as e:
        logger.error('Fatal exception: ' + str(e))
        logger.error(traceback.format_exc())
        triggerEMail( config, "Error starting Pyspark script" )
        sys.exit('Fatal exception: ' + str(e))

if __name__ == "__main__":
    try:
        # read input args
        config_file = sys.argv[1]

        # call run_spark_job outside main to enforce variable scope
        run_spark_job( config_file )

    except IndexError:
        sys.exit("script parameters <config_file.properties>")
