--DROP TABLE etl.control_table_ods;
CREATE TABLE etl.control_table_ods (   
  batch_id INT
, job_name STRING
, batch_load_ts TIMESTAMP
, job_start_ts TIMESTAMP
, job_end_ts TIMESTAMP
, process_runtime_sec INT
, load_status_summary STRING
, load_status_details STRING
, failure_reasons STRING
);


--DROP TABLE etl.load_history_ods;
CREATE TABLE etl.load_history_ods (   
  batch_id INT
, job_name STRING
, batch_load_ts TIMESTAMP
, load_type STRING

, source_db STRING
, source_schema STRING
, source_table STRING
, target_db STRING
, target_table STRING
, incremental_datetime_col STRING
, source_server STRING
, source_port STRING
, source_user STRING
, count_source INT
, count_target_start INT
, count_target_end INT
, count_loaded INT
, process_start_ts TIMESTAMP
, process_end_ts TIMESTAMP
, process_runtime_sec INT
, incr_load_start_ts TIMESTAMP
, incr_load_end_ts TIMESTAMP

, load_status_code STRING
, load_status_desc STRING
, failure_reason STRING
) 



--DROP TABLE etl.control_table_edw;
CREATE TABLE etl.control_table_edw (   
  edw_batch_id INT
, job_name STRING

, stg1_job_start_ts TIMESTAMP
, stg1_job_end_ts TIMESTAMP
, edw_job_start_ts TIMESTAMP
, edw_job_end_ts TIMESTAMP

, load_status_summary STRING
, load_status_details STRING
);

--DROP TABLE etl.load_history_stg1;
CREATE TABLE etl.load_history_stg1 (   
  edw_batch_id INT
, job_name STRING

, table_name STRING
, ods_batch_id_from INT
, ods_batch_id_to INT

, count_loaded INT
, stg1_load_start_ts TIMESTAMP
, stg1_load_end_ts TIMESTAMP
, load_status_code STRING
, load_status_desc STRING
, failure_reason STRING
);