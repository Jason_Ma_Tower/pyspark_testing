from pyspark.sql import SparkSession
from pyspark.sql.functions import lit
from pyspark.sql.types import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import traceback

import time
import os.path
import datetime
import re
import configparser
import logging
import sys, smtplib, cgitb

import threading


def load_table_2_stg( table_info_dict, load_info, config, spark, logger, result_arr ):

    def drop_existing_stg_table( table_info_dict, spark, logger ):
        logger.debug( log_header + "  Entering drop_existing_stg_table" )

        try:
            tgt_table_path = table_info_dict["tgt_table_path"]

            hive_drop_query = f"DROP TABLE IF EXISTS {tgt_table_path}"
            spark.sql(hive_drop_query)
            logger.info(log_header + f"  Dropped existing table {tgt_table_path}")

        except Exception as e:
            logger.error( e )
            error_msg = "Error in drop_existing_stg_table"
            raise Exception( error_msg )

    def populate_stg_table( table_info_dict, load_info, spark, logger ):
        logger.debug(log_header + "  Entering populate_stg_table")
        try:
            src_table_path = table_info_dict["src_table_path"]
            tgt_table_path = table_info_dict["tgt_table_path"]
            batch_id_from  = table_info_dict["ods_batch_id_from"]
            batch_id_to    = table_info_dict["ods_batch_id_to"]
            edw_batch_id   = load_info["edw_batch_id"]

            logger.info(log_header + f"  Loading table {tgt_table_path}")
            hive_create_query = f"""
                CREATE TABLE {tgt_table_path} AS
                SELECT
                    *
                  , {edw_batch_id} AS edw_batch_id
                FROM {src_table_path}
                WHERE 1=1
                    AND ods_batch_id >= {batch_id_from}
                    AND ods_batch_id <= {batch_id_to}
                """
            spark.sql(hive_create_query)

        except Exception as e:
            logger.error( e )
            error_msg = "Error in populate_stg_table"
            raise Exception( error_msg )

    def get_count_loaded( tgt_table_path, spark, logger ):
        logger.debug( log_header + "    Entering get_count_loaded" )

        try:
            hive_count_loaded_query = f"""
                SELECT
                    COUNT(*) as count_loaded
                FROM {tgt_table_path}
                """
            count_loaded = spark.sql( hive_count_loaded_query ).first().count_loaded
            logger.debug( log_header + f"    count_loaded: {count_loaded}" )

            return count_loaded

        except Exception as e:
            logger.error( e )
            error_msg = "Error in get_count_loaded"
            raise Exception( error_msg )

    def update_load_history_table( load_result, load_info, table_info_dict, spark, logger ):
        logger.debug( log_header + "  Entering update_load_history_table" )

        def create_load_row_df( load_result, load_info, table_info_dict, spark, logger ):
            logger.debug(log_header + 'load_history_tuple')

            load_history_tuple = [(
                  load_info.get("edw_batch_id")
                , load_info.get("job_name")

                , table_info_dict.get("table_name")
                , table_info_dict.get("ods_batch_id_from")
                , table_info_dict.get("ods_batch_id_to")
                
                , load_result.get("count_loaded")
                , load_result.get("stg1_load_start_ts")
                , load_result.get("stg1_load_end_ts")
                , load_result.get("load_status")
                , load_result.get("load_status")
                , load_result.get("failure_reason")
                )]
            logger.debug(log_header + str(load_history_tuple) )

            load_history_schema = StructType([
                  StructField("edw_batch_id", IntegerType(), True)
                , StructField("job_name", StringType(), True)

                , StructField("table_name", StringType(), True)
                , StructField("ods_batch_id_from", IntegerType(), True)
                , StructField("ods_batch_id_to", IntegerType(), True)

                , StructField("count_loaded", IntegerType(), True)
                , StructField("stg1_load_start_ts", TimestampType(), True)
                , StructField("stg1_load_end_ts", TimestampType(), True)
                , StructField("load_status_code", StringType(), True)
                , StructField("load_status_desc", StringType(), True)
                , StructField("failure_reason", StringType(), True)
                ])
            return spark.createDataFrame(load_history_tuple, load_history_schema)
        
        try:
            load_row_df = create_load_row_df( load_result, load_info, table_info_dict, spark, logger )
            load_history_table = load_info["load_history_stg1"]

            load_row_df.write \
                .saveAsTable(load_history_table, format="hive", mode="append")

            logger.debug( log_header + "  LOADED load_history_table" )

            return 'LOADED load_history_table'

        except Exception as e:
            error_msg = "Error in update_load_history_table for: " + table_info_dict["tgt_table_path"]
            logger.error(e)
            raise Exception(error_msg)

    load_result = {
          "tgt_table_path"     : "UNKNOWN"
        , "stg1_load_start_ts" : datetime.datetime.now()
        , "load_status"        : "FAILURE"
        , "failure_reason"     : None
        }
    log_header = "UNKNOWN - "

    try:
        log_header = table_info_dict["src_table_path"] + " - "
        logger.debug( log_header + "  Entering load_table_2_stg" )

        drop_existing_stg_table( table_info_dict, spark, logger )
        populate_stg_table( table_info_dict, load_info, spark, logger )

        tgt_table_path = table_info_dict.get("tgt_table_path")
        load_result["stg1_load_end_ts"] = datetime.datetime.now()
        load_result["tgt_table_path"]   = tgt_table_path
        load_result["count_loaded"]     = get_count_loaded( tgt_table_path, spark, logger )
        load_result["load_status"]      = "SUCCESS"

        src_table_path = table_info_dict.get("src_table_path")
        rows_loaded = load_result['count_loaded']
        logger.info( f"  Loaded {rows_loaded} rows from {src_table_path} to {tgt_table_path}" )

    except Exception as e:
        logger.error( log_header + str(e) )
        load_result["failure_reason"] = str(e)

        email_subject = f"Error running ODS-to-STG load for: {table_info_dict['table_name']}"
        triggerEMail( config, email_subject )

    try:
        update_load_history_table( load_result, load_info, table_info_dict, spark, logger )
    except Exception as e:
        error_msg = "Failed to update load history table: " + str(load_info.get("load_history_stg1") )
        logger.error( log_header + error_msg )
        logger.error( log_header + str(e) )

    result_arr.append( load_result )

def update_control_table( result_arr, load_info, config, spark, logger ):
    logger.debug( "Entering update_control_table" )

    def process_result_arr(result_arr, logger):
        logger.debug( "Entering process_result_arr" )

        def get_load_status_summary( result_arr, logger ):
            logger.debug( "Entering get_summary_str" )
            load_status_summary = "UNKNOWN"

            if any( result["load_status"] != "SUCCESS" for result in result_arr):
                load_status_summary = "FAILURE"
            else:
                load_status_summary = "SUCCESS"

            return load_status_summary

        result_dict = {
              "load_status_summary" : get_load_status_summary(result_arr, logger)
            , "load_status_details" : get_load_status_summary(result_arr, logger)
            }
        logger.debug("result_dict")
        logger.debug(result_dict)

        return result_dict

    def create_control_row_df( result_dict, load_info, spark, logger ):
        logger.debug('control_table_tuple')

        ods_batch_id_from = load_info.get("ods_batch_id_from")
        ods_batch_id_to = load_info.get("ods_batch_id_to")

        control_table_data_arr = []

        control_table_tuple = (
              load_info.get("job_name")
            , load_info.get("edw_batch_id")

            , load_info.get("job_start_ts")
            , load_info.get("job_end_ts")
            , None
            , None

            , result_dict.get("load_status_summary")
            , result_dict.get("load_status_details")
            )
        control_table_data_arr.append(control_table_tuple)
        
        logger.debug("control_table_data_arr")
        logger.debug(control_table_data_arr)

        control_table_schema = StructType([
              StructField("job_name", StringType(), True)
            , StructField("edw_batch_id", IntegerType(), True)

            , StructField("stg1_job_start_ts", TimestampType(), True)
            , StructField("stg1_job_end_ts", TimestampType(), True)
            , StructField("edw_job_start_ts", TimestampType(), True)
            , StructField("edw_job_end_ts", TimestampType(), True)

            , StructField("load_status_summary", StringType(), True)
            , StructField("load_status_details", StringType(), True)
            ])

        return spark.createDataFrame(control_table_data_arr, control_table_schema)

    try:
        result_dict = process_result_arr(result_arr, logger)
        control_row_df = create_control_row_df( result_dict, load_info, spark, logger )

        control_row = control_row_df.collect()
        logger.debug('control_row')
        logger.debug(control_row)

        control_table = load_info["control_table"]

        control_row_df.write \
            .saveAsTable(control_table, format="hive", mode="append")

        logger.debug( f"LOADED control_table: {control_table}" )


        job_name    = load_info["job_name"]
        load_status = result_dict["load_status_summary"]
        msg_subject = f"{job_name} ran resulting in {load_status}"
        triggerEMail( config, msg_subject, str(result_arr) )

    except Exception as e:
        logger.error(e)
        error_msg = "Error in update_control_table"
        raise Exception( error_msg )

# ====================================================================================================

def get_load_info( config, spark, logger ):

    def get_edw_batch_id( config, spark, logger ):
        logger.debug( "Entering get_batch_id" )
        try:
            control_table_edw = config['Datalake']['etl.control.table.edw']

            hive_edw_batch_id_query = f'select coalesce(max(edw_batch_id) + 1, 1) as next_batch_id from {control_table_edw}'
            hive_next_batchid_df    = spark.sql( hive_edw_batch_id_query )
            next_batchid            = hive_next_batchid_df.first().next_batch_id

            logger.info("Job loading edw batch id: " + str( next_batchid ) )
            return next_batchid

        except Exception as e:
            error_msg = "Error in get_edw_batch_id"
            logger.error( e )
            raise Exception( error_msg )

    def get_arr_ods_batch_id_from( config, spark, logger ):
        logger.debug( "Entering get_arr_ods_batch_id_from" )

        try:
            load_history_stg1 = config['Datalake']['etl.load.history.stg1']

            hive_ods_batch_id_from_query = f"""
                SELECT
                    table_name
                  , coalesce(max(ods_batch_id_to) OVER(Partition by table_name) + 1, 1) as ods_batch_id_from
                FROM etl.load_history_stg1
                WHERE 1=1
                    AND table_name IS NOT NULL
                    AND load_status_code = "SUCCESS"
                """

            hive_ods_batch_id_from_df = spark.sql( hive_ods_batch_id_from_query )
            ods_batch_id_from_arr     = hive_ods_batch_id_from_df \
                .dropDuplicates() \
                .collect()

            logger.debug("ods_batch_id_from_arr")
            logger.debug(ods_batch_id_from_arr)
            return ods_batch_id_from_arr

        except Exception as e:
            logger.error( e )
            error_msg = "Error in get_arr_ods_batch_id_from"
            raise Exception( error_msg )

    def get_ods_batch_id_to( config, spark, logger ):
        logger.debug( "Entering get_ods_batch_id_to" )
        try:
            control_table_ods = config['Datalake']['etl.control.table.ods']

            hive_ods_batch_id_to_query = f'select coalesce(max(batch_id), 0) as ods_batch_id_to from {control_table_ods}'
            hive_ods_batch_id_to_df    = spark.sql( hive_ods_batch_id_to_query )
            ods_batch_id_to            = hive_ods_batch_id_to_df.first().ods_batch_id_to

            logger.info("Loading ODS batch_id to: " + str( ods_batch_id_to ) )
            return ods_batch_id_to

        except Exception as e:
            logger.error( e )
            error_msg = "Error in get_ods_batch_id_to"
            raise Exception( error_msg )

    try:
        load_info = {
            "job_name" : config['Job']['name']

          , "src_db"     : config['Datalake']['source.database'].lower()
          , "tgt_db"     : config['Datalake']['target.database'].lower()

          , "edw_batch_id"          : get_edw_batch_id( config, spark, logger )
          , "ods_batch_id_to"       : get_ods_batch_id_to( config, spark, logger )
          , "ods_batch_id_from_arr" : get_arr_ods_batch_id_from( config, spark, logger )

          , "control_table"     : config['Datalake']['etl.control.table.edw']
          , "load_history_ods"  : config['Datalake']['etl.load.history.ods']
          , "load_history_stg1" : config['Datalake']['etl.load.history.stg1']
        }

        logger.debug('load_info')
        logger.debug(load_info)

        return load_info

    except Exception as e:
        error_msg = "Error in get_load_info"
        logger.error(error_msg)
        raise Exception(e)

def threaded_load_tables_2_stg( job_start_ts, config, load_info, spark, logger ):
    logger.debug( "Entering threaded_load_tables_2_stg" )

    def get_tables_2_load( load_info, config, spark, logger ):
        logger.debug( "Entering get_tables_2_load" )

        def build_tables_2_import_query( load_info, spark, logger ):
            logger.debug( "Entering build_tables_2_import_query" )

            try:
                load_history_ods  = load_info["load_history_ods"]
                load_history_stg1 = load_info["load_history_stg1"]
                tables_2_import_query = f"""
                    WITH lhs AS 
                    ( SELECT
                          lhs.table_name
                        , MAX( lhs.ods_batch_id_to ) OVER (PARTITION BY lhs.table_name) as max_ods_batch_id
                      FROM {load_history_stg1} lhs
                      WHERE 1=1
                        AND lhs.load_status_code = "SUCCESS"
                    )
                    SELECT
                        lho.target_table as table_name
                    FROM {load_history_ods} lho
                      LEFT JOIN lhs
                        ON lho.target_table = lhs.table_name
                    WHERE 1=1
                      AND lho.batch_id > COALESCE(lhs.max_ods_batch_id, 0)
                      AND lho.load_status_code = "SUCCESS"
                    """
                # logger.debug("tables_2_import_query")
                # logger.debug(tables_2_import_query)
                return tables_2_import_query

            except Exception as e:
                logger.error( e )
                error_msg = "Error in build_tables_2_import_query"
                raise Exception( error_msg )

        def format_tables_info( config, db_tables_w_batch_ids, logger ):
            logger.debug( "Entering format_tables_info" )

            def get_ods_batch_id_from( tgt_table_path, load_info, logger ):
                logger.debug( "Entering get_ods_batch_id_from" )

                batch_id_arr = load_info["ods_batch_id_from_arr"]

                ods_batch_id_from = 1
                for table_batch_id in batch_id_arr:

                    logger.debug("tgt_table_path")
                    logger.debug(tgt_table_path)

                    logger.debug("table_batch_id.table_name")
                    logger.debug(table_batch_id.table_name)

                    if tgt_table_path == table_batch_id.table_name:
                        ods_batch_id_from = table_batch_id.ods_batch_id_from

                logger.debug("ods_batch_id_from")
                logger.debug(ods_batch_id_from)

                return ods_batch_id_from

            try:
                table_info_arr = []

                for table_row in db_tables_w_batch_ids:
                    src_db = config['Datalake']['source.database']
                    tgt_db = config['Datalake']['target.database']
                    src_table_path = f"{src_db}.{table_row.table_name}"
                    tgt_table_path = f"{tgt_db}.{table_row.table_name}"

                    table_info_dict = {
                          "src_table_path" : src_table_path
                        , "tgt_table_path" : tgt_table_path
                        , "table_name"     : table_row.table_name
                        , "ods_batch_id_from" : get_ods_batch_id_from( tgt_table_path, load_info, logger )
                        , "ods_batch_id_to"   : table_row.ods_batch_id_to
                        }

                    table_info_arr.append(table_info_dict)

                return table_info_arr
            except Exception as e:
                logger.error( e )
                error_msg = "Error in format_tables_info"
                raise Exception( error_msg )

        try:
            ods_batch_id_from = load_info.get("ods_batch_id_from")
            ods_batch_id_to   = load_info.get("ods_batch_id_to")

            source_db = config['Datalake']['source.database']

            hive_db_tables_query = build_tables_2_import_query( load_info, spark, logger )
            hive_db_tables_df = spark.sql( hive_db_tables_query )
            db_tables_w_batch_ids = hive_db_tables_df                    \
                .dropDuplicates()                                        \
                .withColumn("ods_batch_id_from", lit(ods_batch_id_from)) \
                .withColumn("ods_batch_id_to", lit(ods_batch_id_to))     \
                .collect()

            tables_2_load_arr = format_tables_info( config, db_tables_w_batch_ids, logger )

            return tables_2_load_arr

        except Exception as e:
            logger.error( e )
            error_msg = "Error in get_tables_2_load"
            raise Exception( error_msg )

    try:
        tables_2_load_arr = get_tables_2_load( load_info, config, spark, logger )

        logger.info('=================================== Loading Tables using threads ===================================')

        thread_arr = []
        result_arr = []
        for table_info_dict in tables_2_load_arr:
            # set thread with parameters then let it run
            load_thread = threading.Thread(target=load_table_2_stg
                , args=(table_info_dict, load_info, config, spark, logger, result_arr) )
            load_thread.start()
            thread_arr.append( load_thread )

        threads_running = True
        while threads_running:
            threads_running = False
            for thread in thread_arr:
                if thread.is_alive():
                    threads_running = True

            if threads_running:
                time.sleep(1)

        load_info["job_start_ts"] = job_start_ts
        load_info["job_end_ts"]   = datetime.datetime.now()

        update_control_table( result_arr, load_info, config, spark, logger )

        logger.info('================================== Finished Loading Tables to ODS ==================================')

    except Exception as e:
        error_msg = "Error in threaded_load_tables_2_stg"
        logger.error( error_msg )
        raise Exception( e )

# ====================================================================================================

def triggerEMail( config, msg_subject, msg_text=None ):

    def sendMail(sender, recipient, subject, html, text):

        message = createMailPart(sender, recipient, subject, html, text)
        server = smtplib.SMTP(config['SMTP']['smtp.host'], config['SMTP']['smtp.port'],  config['SMTP']['smtp.time.out'])
        server.sendmail(sender, recipient, message.as_string())
        server.quit()

    # ---------------------------------------------------------------
    def createMailPart(sender, recipient, subject, html, text):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = recipient

        # Record the MIME types of both parts - text/plain and text/html.
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')

        # the HTML message, is best and preferred.
        msg.attach(part1)
        msg.attach(part2)

        return msg

    def create_hacky_html(msg_subject, msg_text):
        return f"""
        <meta http-equiv="Content-Type" content="text/html; charset=us-ascii"><body bgcolor="#f0f0f8">
        <table width="100%" cellspacing="0" cellpadding="2" border="0" summary="heading">
        <tr bgcolor="#6622aa">
        <td valign="bottom">&nbsp;<br>
        <font color="#ffffff" face="helvetica, arial">&nbsp;<br><big><big><strong>{msg_subject}</strong></big></big></font></td><td align="right" valign="bottom"><font color="#ffffff" face="helvetica, arial">Python 3.6.4: /opt/cloudera/parcels/Anaconda-5.1.0.1/bin/python<br>Thu Feb 28 10:33:39 2019</font></td></tr></table>
            
        <p>{msg_text}</p>

        """

    msg_html = cgitb.html( sys.exc_info() )
    if msg_text is None:
        msg_text = cgitb.text( sys.exc_info() )
    else:
        msg_html = create_hacky_html(msg_subject, msg_text)

    sendMail( config['SMTP']['smtp.sender']
            , config['SMTP']['smtp.recipient']
            , msg_subject
            , msg_html
            , msg_text
            )

# ====================================================================================================

def run_spark_job( config_file ):

    def initialise_logger( config ):

        def log_level(x):
            return {
                'DEBUG': logging.DEBUG,
                'INFO': logging.INFO,
                'WARNING': logging.WARNING,
                'ERROR': logging.ERROR,
                'CRITICAL': logging.CRITICAL
            }.get(x, logging.DEBUG)

        # create logger

        LOG_FILENAME = config['Logging']['logfile.directory'] + '/' + config['Job']['name'] + '-' + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f") + '.log'
        logger = logging.getLogger( config['Job']['name'] )
        logger.setLevel( log_level( config['Logging']['console.level'] ) )

        fh = logging.FileHandler( LOG_FILENAME )
        fh.setLevel( log_level( config['Logging']['logfile.level'] ) )
        ch = logging.StreamHandler()
        ch.setLevel( log_level( config['Logging']['console.level'] ) )

        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        return (logger, LOG_FILENAME)

    def initialise_spark_session( config, logger ):
        logger.debug( "Entering initialise_spark_session" )
        
        job_name = config['Job']['name']
        return (SparkSession
                .builder
                .appName( job_name )
                .config('spark.executor.memory', config['SPARK']['spark.executor.memory'])
                .config('spark.driver.memory', config['SPARK']['spark.driver.memory'])
                .config("spark.cores.max", config['SPARK']['spark.cores.max'])
                .config("spark.sql.parquet.writeLegacyFormat",
                        "true")  # otherwise hive can't read parquet with Decimal(19,2) fields
                .config('spark.yarn.principal', config['SPARK']['Principal'])
                .config('spark.yarn.keytab', config['SPARK']['KeytabFile'])
                .config('spark.default.parallelism' , '4')
                .enableHiveSupport()
                .getOrCreate())

    try:
        job_start_ts = datetime.datetime.now()

        config = configparser.RawConfigParser()
        config.read(config_file)

        (logger, LOG_FILENAME) = initialise_logger( config )
        logger.info('========================================== Initialising ============================================')
        logger.info("Config file: " + config_file)
        logger.info("Logger file: " + LOG_FILENAME)

        spark  = initialise_spark_session( config, logger )

        logger.info('============================== Collecting metadata required for load ===============================')

        load_info = get_load_info( config, spark, logger )

        threaded_load_tables_2_stg( job_start_ts, config, load_info, spark, logger )

    except Exception as e:
        logger.error('Fatal exception: ' + str(e))
        logger.error(traceback.format_exc())

        triggerEMail( config, "Error running Pyspark script" )
        sys.exit('Fatal exception: ' + str(e))

# ====================================================================================================

if __name__ == "__main__":
    try:
        # read input args
        config_file = sys.argv[1]

        # call run_spark_job outside main to enforce variable scope
        run_spark_job( config_file )

    except IndexError:
        sys.exit("script parameters <config_file.properties>")
