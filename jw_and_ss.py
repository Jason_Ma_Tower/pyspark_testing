from pyspark.sql import SparkSession
from dateutil.parser import parse
from pyspark.sql.functions import udf
from pyspark.sql.functions import explode
from pyspark.sql.functions import lit
from pyspark.sql.types import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import traceback

import time
import os.path
import datetime
import re
import configparser
import logging
import sys, smtplib, cgitb

import threading


def get_load_info( config, spark, logger ):

    def get_src_url( config ):
        src_server = config['MSSQL_ODBC']['source.server']
        src_port   = config['MSSQL_ODBC']['source.port']
        src_db     = config['MSSQL_ODBC']['source.database']
        src_args   = config['MSSQL_ODBC']['JDBC.Additional.Args']

        src_url = f"jdbc:jtds:sqlserver://{src_server}:{src_port};databaseName={src_db}{src_args}"

        return src_url

    def get_batch_id( config, spark ):
        logger.debug( "Entering get_batch_id" )
        try:
            etl_control_table = config['Datalake']['etl.control.table']

            hive_batchid_query   = f'select coalesce(max(batch_id) + 1, 1) as next_batch_id from  {etl_control_table}'
            logger.debug( hive_batchid_query )
            hive_next_batch_id_df = spark.sql( hive_batchid_query )
            next_batchid         = hive_next_batch_id_df.first().next_batch_id

            logger.info(f"Current batch id: {next_batchid} __ try this: " + str(next_batchid) )

            return next_batchid

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get batch_id from datalake control table " + etl_control_table
            raise Exception( error_msg )

    def get_batch_load_time( config, mssql_properties ):
        logger.debug( "Entering get_batch_load_time" )
        try:
            src_url = get_src_url( config )

            # setup MS JDBC connection
            incremental_delay     = config['Incremental']['incremental.time.delay.min']
            logger.debug( "  Time Window Difference in min: " + incremental_delay )
            mssql_date_time_query = f"""
                (
                    SELECT
                        CONVERT(DATETIME2(0), dateadd(minute, - {incremental_delay}, getdate()) ) as batch_load_datetime
                ) batch_load_datetime
                """
            mssql_curDateTime_df  = spark.read.jdbc(url = src_url, table = mssql_date_time_query, properties = mssql_properties)

            batch_load_ts_str = mssql_curDateTime_df.first().batch_load_datetime
            batch_load_ts = datetime.datetime.strptime(batch_load_ts_str, '%Y-%m-%d %H:%M:%S')
            logger.info("Current batch load end time: " + str( batch_load_ts ) )

            return batch_load_ts

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get batch_load_ts from source_db" + src_url
            raise Exception( error_msg )

    def get_latest_load_time_for_all_tables( config, spark ):
        logger.debug( "Entering get_latest_load_time_for_all_tables" )
        try:
            load_history_table                = config['Datalake']['etl.load.history']
            hive_latest_table_load_time_query = f"""
                SELECT
                    target_table
                  , max(incr_load_end_ts) as latest_loadtime --jason these two bugs are subtle, second will only be picked up when tested when loading multiple tables OVER(Partition by target_table) as latest_incr_loadtime
                FROM {load_history_table}
                WHERE 1=1
                    AND target_table is not null
                    AND load_status_code = "SUCCESS"
                """
            logger.debug("hive_latest_table_load_time_query")
            logger.debug(hive_latest_table_load_time_query)
            hive_latest_load_times_all_tables_df = spark.sql( hive_latest_table_load_time_query )
            hive_latest_load_times_all_tables_df.dropDuplicates()
            latest_load_times = hive_latest_load_times_all_tables_df.collect()

            return latest_load_times

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get latest batch load times from datalake control table " + load_history_table
            raise Exception( error_msg )

    def get_existing_tablenames( config, spark ):
        logger.debug( "Entering get_existing_tablenames" )
        try:
            hive_get_tables_query   = f"SHOW TABLES FROM {config['Datalake']['target.database']}"
            hive_existing_tables_df = spark.sql( hive_get_tables_query )
            tablenames_rdd          = hive_existing_tables_df.rdd.map( lambda row: row.tableName )
            existing_tables_arr = tablenames_rdd

            return existing_tables_arr

        except Exception as e:
            logger.error( e )
            error_msg = "Unable to get existing tables"
            raise Exception( error_msg )

    logger.info('============================== Collecting metadata required for load ===============================')

    try:
        mssql_properties = {
            "user"     : config['MSSQL_ODBC']['User']
          , "password" : config['MSSQL_ODBC']['Password']
          , "driver"   : config['MSSQL_ODBC']['JDBC.Driver.Name']
        }

        load_info = {
            "mssql_properties" : mssql_properties
          , "job_name"         : config['Job']['name']

          , "src_url"    : get_src_url( config )
          , "src_server" : config['MSSQL_ODBC']['source.server']
          , "src_port"   : config['MSSQL_ODBC']['source.port']
          , "src_db"     : config['MSSQL_ODBC']['source.database']
          , "tgt_db"     : config['Datalake']['target.database'].lower()

          , "batch_id"            : get_batch_id( config, spark )
          , "batch_load_ts"       : get_batch_load_time( config, mssql_properties )
          , "latest_load_times"   : get_latest_load_time_for_all_tables( config, spark )
          , "existing_tables_arr" : get_existing_tablenames( config, spark )

          , "control_table"      : config['Datalake']['etl.control.table']
          , "load_history_table" : config['Datalake']['etl.load.history']
          , "source_user"        : config['MSSQL_ODBC']['User']
        }

        logger.debug('load_info')
        logger.debug(load_info)

        logger.info('=================================== Finished collecting metadata ===================================')

        return load_info

    except Exception as e:
        error_msg = "Unable to retrieve metadata for batch load"
        logger.error(error_msg)
        logger.error(e)
        raise Exception(error_msg)


def run_spark_job( config_file ):

    def initialise_logger( config ):

        def log_level(x):
            return {
                'DEBUG': logging.DEBUG,
                'INFO': logging.INFO,
                'WARNING': logging.WARNING,
                'ERROR': logging.ERROR,
                'CRITICAL': logging.CRITICAL
            }.get(x, logging.DEBUG)

        # create logger

        LOG_FILENAME = config['Logging']['logfile.directory'] + '/' + config['Src_Tgt_Mapping']['mapping.filename'] + '-' + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f") + '.log'
        logger = logging.getLogger( config['Job']['name'] )
        logger.setLevel( log_level( config['Logging']['console.level'] ) )

        fh = logging.FileHandler( LOG_FILENAME )
        fh.setLevel( log_level( config['Logging']['logfile.level'] ) )
        ch = logging.StreamHandler()
        ch.setLevel( log_level( config['Logging']['console.level'] ) )

        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        return (logger, LOG_FILENAME)

    def initialise_spark_session( config ):
        logger.debug( "Entering initialise_spark_session" )
        jdbc_driver_path = config['MSSQL_ODBC']['JDBC.Driver.Path']
        job_name         = config['Job']['name']

        return (SparkSession
                .builder
                .appName( job_name )
                .config('spark.executor.memory', config['SPARK']['spark.executor.memory'])
                .config('spark.driver.memory', config['SPARK']['spark.driver.memory'])
                .config("spark.cores.max", config['SPARK']['spark.cores.max'])
                .config("spark.sql.parquet.writeLegacyFormat",
                        "true")  # otherwise hive can't read parquet with Decimal(19,2) fields
                .config('spark.driver.extraClassPath', jdbc_driver_path)
                .config('spark.executor.extraClassPath', jdbc_driver_path)
                .config('spark.yarn.principal', config['SPARK']['Principal'])
                .config('spark.yarn.keytab', config['SPARK']['KeytabFile'])
                .config('spark.default.parallelism' , '4')
                .enableHiveSupport()
                .getOrCreate())

    try:
        job_start_ts = datetime.datetime.now()

        config = configparser.RawConfigParser()
        config.read(config_file)

        (logger, LOG_FILENAME) = initialise_logger( config )
        logger.info('========================================== Initialising ============================================')
        logger.info("Config file: " + config_file)
        logger.info("Logger file: " + LOG_FILENAME)

        spark  = initialise_spark_session( config )

        load_info = get_load_info( config, spark, logger )

        # threaded_load_tables_2_ods( job_start_ts, config, load_info, spark, logger )

    except Exception as e:
        logger.error('Fatal exception: ' + str(e))
        logger.error(traceback.format_exc())
        #triggerEMail( config, "Error starting Pyspark script" )
        sys.exit('Fatal exception: ' + str(e))


if __name__ == "__main__":
    try:
        # read input args
        config_file = sys.argv[1]

        # call run_spark_job outside main to enforce variable scope
        run_spark_job( config_file )

    except IndexError:
        sys.exit("script parameters <config_file.properties>")
